/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jsam4
 */
@Entity
@Table(name = "pedido_sucursales")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PedidoSucursales.findAll", query = "SELECT p FROM PedidoSucursales p")
    , @NamedQuery(name = "PedidoSucursales.findByPedidonumeroFactura", query = "SELECT p FROM PedidoSucursales p WHERE p.pedidoSucursalesPK.pedidonumeroFactura = :pedidonumeroFactura")
    , @NamedQuery(name = "PedidoSucursales.findBySucusalesidSucursal", query = "SELECT p FROM PedidoSucursales p WHERE p.pedidoSucursalesPK.sucusalesidSucursal = :sucusalesidSucursal")
    , @NamedQuery(name = "PedidoSucursales.findByIdProducto", query = "SELECT p FROM PedidoSucursales p WHERE p.idProducto = :idProducto")
    , @NamedQuery(name = "PedidoSucursales.findByCantidadProductoCola", query = "SELECT p FROM PedidoSucursales p WHERE p.cantidadProductoCola = :cantidadProductoCola")
    , @NamedQuery(name = "PedidoSucursales.findByNombreProductoCola", query = "SELECT p FROM PedidoSucursales p WHERE p.nombreProductoCola = :nombreProductoCola")})
public class PedidoSucursales implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PedidoSucursalesPK pedidoSucursalesPK;
    @Basic(optional = false)
    @Column(name = "id_producto")
    private long idProducto;
    @Basic(optional = false)
    @Column(name = "cantidad_producto_cola")
    private int cantidadProductoCola;
    @Basic(optional = false)
    @Column(name = "nombre_producto_cola")
    private String nombreProductoCola;
    @JoinColumn(name = "pedidonumero_factura", referencedColumnName = "id_factura", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Factura factura;
    @JoinColumn(name = "sucusalesid_sucursal", referencedColumnName = "id_sucursal", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Sucursales sucursales;

    public PedidoSucursales() {
    }

    public PedidoSucursales(PedidoSucursalesPK pedidoSucursalesPK) {
        this.pedidoSucursalesPK = pedidoSucursalesPK;
    }

    public PedidoSucursales(PedidoSucursalesPK pedidoSucursalesPK, long idProducto, int cantidadProductoCola, String nombreProductoCola) {
        this.pedidoSucursalesPK = pedidoSucursalesPK;
        this.idProducto = idProducto;
        this.cantidadProductoCola = cantidadProductoCola;
        this.nombreProductoCola = nombreProductoCola;
    }

    public PedidoSucursales(long pedidonumeroFactura, long sucusalesidSucursal) {
        this.pedidoSucursalesPK = new PedidoSucursalesPK(pedidonumeroFactura, sucusalesidSucursal);
    }

    public PedidoSucursalesPK getPedidoSucursalesPK() {
        return pedidoSucursalesPK;
    }

    public void setPedidoSucursalesPK(PedidoSucursalesPK pedidoSucursalesPK) {
        this.pedidoSucursalesPK = pedidoSucursalesPK;
    }

    public long getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(long idProducto) {
        this.idProducto = idProducto;
    }

    public int getCantidadProductoCola() {
        return cantidadProductoCola;
    }

    public void setCantidadProductoCola(int cantidadProductoCola) {
        this.cantidadProductoCola = cantidadProductoCola;
    }

    public String getNombreProductoCola() {
        return nombreProductoCola;
    }

    public void setNombreProductoCola(String nombreProductoCola) {
        this.nombreProductoCola = nombreProductoCola;
    }

    public Factura getFactura() {
        return factura;
    }

    public void setFactura(Factura factura) {
        this.factura = factura;
    }

    public Sucursales getSucursales() {
        return sucursales;
    }

    public void setSucursales(Sucursales sucursales) {
        this.sucursales = sucursales;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pedidoSucursalesPK != null ? pedidoSucursalesPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PedidoSucursales)) {
            return false;
        }
        PedidoSucursales other = (PedidoSucursales) object;
        if ((this.pedidoSucursalesPK == null && other.pedidoSucursalesPK != null) || (this.pedidoSucursalesPK != null && !this.pedidoSucursalesPK.equals(other.pedidoSucursalesPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.PedidoSucursales[ pedidoSucursalesPK=" + pedidoSucursalesPK + " ]";
    }
    
}
