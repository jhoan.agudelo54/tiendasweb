/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jsam4
 */
@Entity
@Table(name = "proveedor_sucursales")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProveedorSucursales.findAll", query = "SELECT p FROM ProveedorSucursales p")
    , @NamedQuery(name = "ProveedorSucursales.findByProveedorIdProveedor", query = "SELECT p FROM ProveedorSucursales p WHERE p.proveedorSucursalesPK.proveedorIdProveedor = :proveedorIdProveedor")
    , @NamedQuery(name = "ProveedorSucursales.findBySucursalesIdSucursal", query = "SELECT p FROM ProveedorSucursales p WHERE p.proveedorSucursalesPK.sucursalesIdSucursal = :sucursalesIdSucursal")
    , @NamedQuery(name = "ProveedorSucursales.findByFechaCompra", query = "SELECT p FROM ProveedorSucursales p WHERE p.fechaCompra = :fechaCompra")
    , @NamedQuery(name = "ProveedorSucursales.findByHoraCompra", query = "SELECT p FROM ProveedorSucursales p WHERE p.horaCompra = :horaCompra")
    , @NamedQuery(name = "ProveedorSucursales.findByNombreProducto", query = "SELECT p FROM ProveedorSucursales p WHERE p.nombreProducto = :nombreProducto")})
public class ProveedorSucursales implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ProveedorSucursalesPK proveedorSucursalesPK;
    @Basic(optional = false)
    @Column(name = "fecha_compra")
    @Temporal(TemporalType.DATE)
    private Date fechaCompra;
    @Basic(optional = false)
    @Column(name = "hora_compra")
    @Temporal(TemporalType.TIME)
    private Date horaCompra;
    @Basic(optional = false)
    @Column(name = "nombre_producto")
    private String nombreProducto;
    @JoinColumn(name = "proveedor_id_proveedor", referencedColumnName = "id_proveedore", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Proveedores proveedores;
    @JoinColumn(name = "sucursales_id_sucursal", referencedColumnName = "id_sucursal", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Sucursales sucursales;

    public ProveedorSucursales() {
    }

    public ProveedorSucursales(ProveedorSucursalesPK proveedorSucursalesPK) {
        this.proveedorSucursalesPK = proveedorSucursalesPK;
    }

    public ProveedorSucursales(ProveedorSucursalesPK proveedorSucursalesPK, Date fechaCompra, Date horaCompra, String nombreProducto) {
        this.proveedorSucursalesPK = proveedorSucursalesPK;
        this.fechaCompra = fechaCompra;
        this.horaCompra = horaCompra;
        this.nombreProducto = nombreProducto;
    }

    public ProveedorSucursales(long proveedorIdProveedor, long sucursalesIdSucursal) {
        this.proveedorSucursalesPK = new ProveedorSucursalesPK(proveedorIdProveedor, sucursalesIdSucursal);
    }

    public ProveedorSucursalesPK getProveedorSucursalesPK() {
        return proveedorSucursalesPK;
    }

    public void setProveedorSucursalesPK(ProveedorSucursalesPK proveedorSucursalesPK) {
        this.proveedorSucursalesPK = proveedorSucursalesPK;
    }

    public Date getFechaCompra() {
        return fechaCompra;
    }

    public void setFechaCompra(Date fechaCompra) {
        this.fechaCompra = fechaCompra;
    }

    public Date getHoraCompra() {
        return horaCompra;
    }

    public void setHoraCompra(Date horaCompra) {
        this.horaCompra = horaCompra;
    }

    public String getNombreProducto() {
        return nombreProducto;
    }

    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    public Proveedores getProveedores() {
        return proveedores;
    }

    public void setProveedores(Proveedores proveedores) {
        this.proveedores = proveedores;
    }

    public Sucursales getSucursales() {
        return sucursales;
    }

    public void setSucursales(Sucursales sucursales) {
        this.sucursales = sucursales;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (proveedorSucursalesPK != null ? proveedorSucursalesPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProveedorSucursales)) {
            return false;
        }
        ProveedorSucursales other = (ProveedorSucursales) object;
        if ((this.proveedorSucursalesPK == null && other.proveedorSucursalesPK != null) || (this.proveedorSucursalesPK != null && !this.proveedorSucursalesPK.equals(other.proveedorSucursalesPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.ProveedorSucursales[ proveedorSucursalesPK=" + proveedorSucursalesPK + " ]";
    }
    
}
