/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author jsam4
 */
@Embeddable
public class ProveedorSucursalesPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "proveedor_id_proveedor")
    private long proveedorIdProveedor;
    @Basic(optional = false)
    @Column(name = "sucursales_id_sucursal")
    private long sucursalesIdSucursal;

    public ProveedorSucursalesPK() {
    }

    public ProveedorSucursalesPK(long proveedorIdProveedor, long sucursalesIdSucursal) {
        this.proveedorIdProveedor = proveedorIdProveedor;
        this.sucursalesIdSucursal = sucursalesIdSucursal;
    }

    public long getProveedorIdProveedor() {
        return proveedorIdProveedor;
    }

    public void setProveedorIdProveedor(long proveedorIdProveedor) {
        this.proveedorIdProveedor = proveedorIdProveedor;
    }

    public long getSucursalesIdSucursal() {
        return sucursalesIdSucursal;
    }

    public void setSucursalesIdSucursal(long sucursalesIdSucursal) {
        this.sucursalesIdSucursal = sucursalesIdSucursal;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) proveedorIdProveedor;
        hash += (int) sucursalesIdSucursal;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProveedorSucursalesPK)) {
            return false;
        }
        ProveedorSucursalesPK other = (ProveedorSucursalesPK) object;
        if (this.proveedorIdProveedor != other.proveedorIdProveedor) {
            return false;
        }
        if (this.sucursalesIdSucursal != other.sucursalesIdSucursal) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.ProveedorSucursalesPK[ proveedorIdProveedor=" + proveedorIdProveedor + ", sucursalesIdSucursal=" + sucursalesIdSucursal + " ]";
    }
    
}
