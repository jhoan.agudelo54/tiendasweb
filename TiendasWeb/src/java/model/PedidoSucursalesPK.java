/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author jsam4
 */
@Embeddable
public class PedidoSucursalesPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "pedidonumero_factura")
    private long pedidonumeroFactura;
    @Basic(optional = false)
    @Column(name = "sucusalesid_sucursal")
    private long sucusalesidSucursal;

    public PedidoSucursalesPK() {
    }

    public PedidoSucursalesPK(long pedidonumeroFactura, long sucusalesidSucursal) {
        this.pedidonumeroFactura = pedidonumeroFactura;
        this.sucusalesidSucursal = sucusalesidSucursal;
    }

    public long getPedidonumeroFactura() {
        return pedidonumeroFactura;
    }

    public void setPedidonumeroFactura(long pedidonumeroFactura) {
        this.pedidonumeroFactura = pedidonumeroFactura;
    }

    public long getSucusalesidSucursal() {
        return sucusalesidSucursal;
    }

    public void setSucusalesidSucursal(long sucusalesidSucursal) {
        this.sucusalesidSucursal = sucusalesidSucursal;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) pedidonumeroFactura;
        hash += (int) sucusalesidSucursal;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PedidoSucursalesPK)) {
            return false;
        }
        PedidoSucursalesPK other = (PedidoSucursalesPK) object;
        if (this.pedidonumeroFactura != other.pedidonumeroFactura) {
            return false;
        }
        if (this.sucusalesidSucursal != other.sucusalesidSucursal) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.PedidoSucursalesPK[ pedidonumeroFactura=" + pedidonumeroFactura + ", sucusalesidSucursal=" + sucusalesidSucursal + " ]";
    }
    
}
