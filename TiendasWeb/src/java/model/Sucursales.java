/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author jsam4
 */
@Entity
@Table(name = "sucursales")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Sucursales.findAll", query = "SELECT s FROM Sucursales s")
    , @NamedQuery(name = "Sucursales.findByIdSucursal", query = "SELECT s FROM Sucursales s WHERE s.idSucursal = :idSucursal")
    , @NamedQuery(name = "Sucursales.findByNombreSucursal", query = "SELECT s FROM Sucursales s WHERE s.nombreSucursal = :nombreSucursal")
    , @NamedQuery(name = "Sucursales.findByCiudad", query = "SELECT s FROM Sucursales s WHERE s.ciudad = :ciudad")
    , @NamedQuery(name = "Sucursales.findByDireccion", query = "SELECT s FROM Sucursales s WHERE s.direccion = :direccion")
    , @NamedQuery(name = "Sucursales.findByTelefono", query = "SELECT s FROM Sucursales s WHERE s.telefono = :telefono")
    , @NamedQuery(name = "Sucursales.findByTotalVentas", query = "SELECT s FROM Sucursales s WHERE s.totalVentas = :totalVentas")})
public class Sucursales implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_sucursal")
    private Long idSucursal;
    @Basic(optional = false)
    @Column(name = "nombre_sucursal")
    private String nombreSucursal;
    @Basic(optional = false)
    @Column(name = "ciudad")
    private String ciudad;
    @Basic(optional = false)
    @Column(name = "direccion")
    private String direccion;
    @Basic(optional = false)
    @Column(name = "telefono")
    private String telefono;
    @Basic(optional = false)
    @Column(name = "total_ventas")
    private long totalVentas;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "sucursalesIdSucursal")
    private Collection<Proveedores> proveedoresCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "sucursales")
    private Collection<PedidoSucursales> pedidoSucursalesCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "sucursalesIdSucursal")
    private Collection<Inventario> inventarioCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "sucursales")
    private Collection<ProveedorSucursales> proveedorSucursalesCollection;

    public Sucursales() {
    }

    public Sucursales(Long idSucursal) {
        this.idSucursal = idSucursal;
    }

    public Sucursales(Long idSucursal, String nombreSucursal, String ciudad, String direccion, String telefono, long totalVentas) {
        this.idSucursal = idSucursal;
        this.nombreSucursal = nombreSucursal;
        this.ciudad = ciudad;
        this.direccion = direccion;
        this.telefono = telefono;
        this.totalVentas = totalVentas;
    }

    public Long getIdSucursal() {
        return idSucursal;
    }

    public void setIdSucursal(Long idSucursal) {
        this.idSucursal = idSucursal;
    }

    public String getNombreSucursal() {
        return nombreSucursal;
    }

    public void setNombreSucursal(String nombreSucursal) {
        this.nombreSucursal = nombreSucursal;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public long getTotalVentas() {
        return totalVentas;
    }

    public void setTotalVentas(long totalVentas) {
        this.totalVentas = totalVentas;
    }

    @XmlTransient
    public Collection<Proveedores> getProveedoresCollection() {
        return proveedoresCollection;
    }

    public void setProveedoresCollection(Collection<Proveedores> proveedoresCollection) {
        this.proveedoresCollection = proveedoresCollection;
    }

    @XmlTransient
    public Collection<PedidoSucursales> getPedidoSucursalesCollection() {
        return pedidoSucursalesCollection;
    }

    public void setPedidoSucursalesCollection(Collection<PedidoSucursales> pedidoSucursalesCollection) {
        this.pedidoSucursalesCollection = pedidoSucursalesCollection;
    }

    @XmlTransient
    public Collection<Inventario> getInventarioCollection() {
        return inventarioCollection;
    }

    public void setInventarioCollection(Collection<Inventario> inventarioCollection) {
        this.inventarioCollection = inventarioCollection;
    }

    @XmlTransient
    public Collection<ProveedorSucursales> getProveedorSucursalesCollection() {
        return proveedorSucursalesCollection;
    }

    public void setProveedorSucursalesCollection(Collection<ProveedorSucursales> proveedorSucursalesCollection) {
        this.proveedorSucursalesCollection = proveedorSucursalesCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSucursal != null ? idSucursal.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sucursales)) {
            return false;
        }
        Sucursales other = (Sucursales) object;
        if ((this.idSucursal == null && other.idSucursal != null) || (this.idSucursal != null && !this.idSucursal.equals(other.idSucursal))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Sucursales[ idSucursal=" + idSucursal + " ]";
    }
    
}
