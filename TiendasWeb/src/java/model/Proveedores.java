/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author jsam4
 */
@Entity
@Table(name = "proveedores")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Proveedores.findAll", query = "SELECT p FROM Proveedores p")
    , @NamedQuery(name = "Proveedores.findByIdProveedore", query = "SELECT p FROM Proveedores p WHERE p.idProveedore = :idProveedore")
    , @NamedQuery(name = "Proveedores.findByNombreProveedor", query = "SELECT p FROM Proveedores p WHERE p.nombreProveedor = :nombreProveedor")
    , @NamedQuery(name = "Proveedores.findByNombreProducto", query = "SELECT p FROM Proveedores p WHERE p.nombreProducto = :nombreProducto")
    , @NamedQuery(name = "Proveedores.findByPrecioProducto", query = "SELECT p FROM Proveedores p WHERE p.precioProducto = :precioProducto")
    , @NamedQuery(name = "Proveedores.findByTelefono", query = "SELECT p FROM Proveedores p WHERE p.telefono = :telefono")})
public class Proveedores implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_proveedore")
    private Long idProveedore;
    @Basic(optional = false)
    @Column(name = "nombre_proveedor")
    private String nombreProveedor;
    @Basic(optional = false)
    @Column(name = "nombre_producto")
    private String nombreProducto;
    @Basic(optional = false)
    @Column(name = "precio_producto")
    private long precioProducto;
    @Column(name = "telefono")
    private String telefono;
    @JoinColumn(name = "sucursales_id_sucursal", referencedColumnName = "id_sucursal")
    @ManyToOne(optional = false)
    private Sucursales sucursalesIdSucursal;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "proveedores")
    private Collection<ProveedorSucursales> proveedorSucursalesCollection;

    public Proveedores() {
    }

    public Proveedores(Long idProveedore) {
        this.idProveedore = idProveedore;
    }

    public Proveedores(Long idProveedore, String nombreProveedor, String nombreProducto, long precioProducto) {
        this.idProveedore = idProveedore;
        this.nombreProveedor = nombreProveedor;
        this.nombreProducto = nombreProducto;
        this.precioProducto = precioProducto;
    }

    public Long getIdProveedore() {
        return idProveedore;
    }

    public void setIdProveedore(Long idProveedore) {
        this.idProveedore = idProveedore;
    }

    public String getNombreProveedor() {
        return nombreProveedor;
    }

    public void setNombreProveedor(String nombreProveedor) {
        this.nombreProveedor = nombreProveedor;
    }

    public String getNombreProducto() {
        return nombreProducto;
    }

    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    public long getPrecioProducto() {
        return precioProducto;
    }

    public void setPrecioProducto(long precioProducto) {
        this.precioProducto = precioProducto;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Sucursales getSucursalesIdSucursal() {
        return sucursalesIdSucursal;
    }

    public void setSucursalesIdSucursal(Sucursales sucursalesIdSucursal) {
        this.sucursalesIdSucursal = sucursalesIdSucursal;
    }

    @XmlTransient
    public Collection<ProveedorSucursales> getProveedorSucursalesCollection() {
        return proveedorSucursalesCollection;
    }

    public void setProveedorSucursalesCollection(Collection<ProveedorSucursales> proveedorSucursalesCollection) {
        this.proveedorSucursalesCollection = proveedorSucursalesCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProveedore != null ? idProveedore.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Proveedores)) {
            return false;
        }
        Proveedores other = (Proveedores) object;
        if ((this.idProveedore == null && other.idProveedore != null) || (this.idProveedore != null && !this.idProveedore.equals(other.idProveedore))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Proveedores[ idProveedore=" + idProveedore + " ]";
    }
    
}
